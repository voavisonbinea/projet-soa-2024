import Sequelize from "sequelize";
import dotenv from "dotenv";

dotenv.config(); // Load environment variables from .env file

export const initStudent = () => {
  console.log("DB_NAME: ", process.env.DB_NAME);
  const sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USER,
    process.env.DB_PASSWORD,
    {
      host: process.env.DB_HOST,
      dialect: "postgres",
    }
  );

  const Student = sequelize.define(
    "student",
    {
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      class: {
        type: Sequelize.STRING,
        allowNull: false,
      },
    },
    {
      // Use the `id` field as the primary key
      primaryKey: true,
    }
  );

  sequelize.sync(); // Create the table in the database
  return Student;
};
