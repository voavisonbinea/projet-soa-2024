import express from "express";
import cors from "cors";
import { initStudent } from "./student.js";
import { validateStudent } from "./validation.js";

const app = express();

app.use(cors());
app.use(express.json());

const Student = initStudent();

// Define the route handlers
const createStudent = async (req, res) => {
  const validationResult = validateStudent(req.body);

  if (validationResult.error) {
    return res
      .status(400)
      .json({ message: validationResult.error.details[0].message });
  }
  const student = await Student.create(req.body);
  res.status(201).json(student);
};

const getStudents = async (req, res) => {
  const students = await Student.findAll();
  res.json(students);
};

const getStudent = async (req, res) => {
  const student = await Student.findByPk(req.params.id);
  if (student) {
    res.json(student);
  } else {
    res.sendStatus(404);
  }
};

const updateStudent = async (req, res) => {
  const student = await Student.findByPk(req.params.id);
  if (student) {
    const validationResult = validateStudent(req.body);

    if (validationResult.error) {
      return res
        .status(400)
        .json({ message: validationResult.error.details[0].message });
    }
    await student.update(req.body);
    res.json(student);
  } else {
    res.sendStatus(404);
  }
};

const deleteStudent = async (req, res) => {
  const student = await Student.findByPk(req.params.id);
  if (student) {
    await student.destroy();
    res.sendStatus(204);
  } else {
    res.sendStatus(404);
  }
};

app.post("/", createStudent);
app.get("/", getStudents);
app.get("/:id", getStudent);
app.put("/:id", updateStudent);
app.delete("/:id", deleteStudent);

// Get the port from an environment variable or use a default value
const port = 4000;

// Start the server
app.listen(port, () => {
  console.log(`server launched on port ${port}`);
});
