import Joi from "joi";

const studentSchema = Joi.object({
  name: Joi.string().min(3).max(50).required(),
  email: Joi.string().email().required(),
  class: Joi.string().required(),
});

export const validateStudent = (student) => {
  return studentSchema.validate(student);
};
