import express from "express";
import cors from "cors";
import { initGrade } from "./grades.js";
import { validateGrade } from "./validation.js";

const app = express();

app.use(cors());
app.use(express.json());

const Grade = initGrade();

// Define the route handlers
const createGrade = async (req, res) => {
  const validationResult = validateGrade(req.body);

  if (validationResult.error) {
    return res
      .status(400)
      .json({ message: validationResult.error.details[0].message });
  }
  const grade = await Grade.create(req.body);
  res.status(201).json(grade);
};

const getGrades = async (req, res) => {
  const grades = await Grade.findAll();
  res.json(grades);
};

const getGrade = async (req, res) => {
  const grade = await Grade.findByPk(req.params.id);
  if (grade) {
    res.json(grade);
  } else {
    res.sendStatus(404);
  }
};

const updateGrade = async (req, res) => {
  const grade = await Grade.findByPk(req.params.id);
  if (grade) {
    const validationResult = validateGrade(req.body);

    if (validationResult.error) {
      return res
        .status(400)
        .json({ message: validationResult.error.details[0].message });
    }
    await grade.update(req.body);
    res.json(grade);
  } else {
    res.sendStatus(404);
  }
};

const deleteGrade = async (req, res) => {
  const grade = await Grade.findByPk(req.params.id);
  if (grade) {
    await grade.destroy();
    res.sendStatus(204);
  } else {
    res.sendStatus(404);
  }
};

app.post("/", createGrade);
app.get("/", getGrades);
app.get("/:id", getGrade);
app.put("/:id", updateGrade);
app.delete("/:id", deleteGrade);

// Get the port from an environment variable or use a default value
const port = 4000;

// Start the server
app.listen(port, () => {
  console.log(`server launched on port ${port}`);
});
