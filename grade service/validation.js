import Joi from "joi";

const gradeSchema = Joi.object({
  value: Joi.number().precision(2).min(0).max(20).required(),
  student_id: Joi.number().integer().min(1).required(),
  student_name: Joi.string().allow(null, "").optional(),
  course_id: Joi.number().integer().min(1).required(),
  course_name: Joi.string().allow(null, "").optional(),
});

export const validateGrade = (grade) => {
  return gradeSchema.validate(grade);
};
