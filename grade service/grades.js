import Sequelize from "sequelize";
import dotenv from "dotenv";

dotenv.config(); // Load environment variables from .env file

export const initGrade = () => {
  console.log("DB_NAME: ", process.env.DB_NAME);
  const sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USER,
    process.env.DB_PASSWORD,
    {
      host: process.env.DB_HOST,
      dialect: "postgres",
    }
  );

  const Grade = sequelize.define(
    "grade",
    {
      value: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      student_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      student_name: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      course_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      course_name: {
        type: Sequelize.STRING,
        allowNull: true,
      },
    },
    {
      // Use the `id` field as the primary key
      primaryKey: true,
    }
  );

  sequelize.sync(); // Create the table in the database
  return Grade;
};
