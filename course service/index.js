import express from "express";
import cors from "cors";
import { initCourse } from "./course.js";
import { validateCourse } from "./validation.js";

const app = express();

app.use(cors());
app.use(express.json());

const Course = initCourse();

// Define the route handlers
const createCourse = async (req, res) => {
  const validationResult = validateCourse(req.body);

  if (validationResult.error) {
    return res
      .status(400)
      .json({ message: validationResult.error.details[0].message });
  }
  const course = await Course.create(req.body);
  res.status(201).json(course);
};

const getCourses = async (req, res) => {
  const courses = await Course.findAll();
  res.json(courses);
};

const getCourse = async (req, res) => {
  const course = await Course.findByPk(req.params.id);
  if (course) {
    res.json(course);
  } else {
    res.sendStatus(404);
  }
};

const updateCourse = async (req, res) => {
  const course = await Course.findByPk(req.params.id);
  if (course) {
    const validationResult = validateCourse(req.body);

    if (validationResult.error) {
      return res
        .status(400)
        .json({ message: validationResult.error.details[0].message });
    }
    await course.update(req.body);
    res.json(course);
  } else {
    res.sendStatus(404);
  }
};

const deleteCourse = async (req, res) => {
  const course = await Course.findByPk(req.params.id);
  if (course) {
    await course.destroy();
    res.sendStatus(204);
  } else {
    res.sendStatus(404);
  }
};

app.post("/", createCourse);
app.get("/", getCourses);
app.get("/:id", getCourse);
app.put("/:id", updateCourse);
app.delete("/:id", deleteCourse);

// Get the port from an environment variable or use a default value
const port = 4000;

// Start the server
app.listen(port, () => {
  console.log(`server launched on port ${port}`);
});
