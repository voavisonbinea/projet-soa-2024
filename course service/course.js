import Sequelize from "sequelize";
import dotenv from "dotenv";

dotenv.config(); // Load environment variables from .env file

export const initCourse = () => {
  console.log("DB_NAME: ", process.env.DB_NAME);
  const sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USER,
    process.env.DB_PASSWORD,
    {
      host: process.env.DB_HOST,
      dialect: "postgres",
    }
  );

  const Course = sequelize.define(
    "course",
    {
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      description: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      coeff: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
    },
    {
      // Use the `id` field as the primary key
      primaryKey: true,
    }
  );

  sequelize.sync(); // Create the table in the database
  return Course;
};
