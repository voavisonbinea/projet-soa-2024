import Joi from "joi";

const courseSchema = Joi.object({
  name: Joi.string().min(3).max(50).required(),
  description: Joi.string().allow(null, "").optional(),
  coeff: Joi.number().integer().min(1).required(),
});

export const validateCourse = (course) => {
  return courseSchema.validate(course);
};
